#!/bin/bash
# strict mode
set -uo pipefail

#######################################
# Globalni promenne
#######################################
readonly GIT_ANDROID_REPOSITORY="git@bitbucket.org:deepvisioncz/doveze.cz-android.git"
readonly GIT_ANDROID_BRANCH_NAME="master"
readonly GIT_IOS_REPOSITORY="git@bitbucket.org:deepvisioncz/doveze.cz-ios.git"
readonly GIT_IOS_BRANCH_NAME="master"

readonly PROGDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly TOPAZ_INPUT_DIR="${PROGDIR}/input"
readonly TOPAZ_CONFIG_FILE="${TOPAZ_INPUT_DIR}/topaz_config.csv"
readonly CURRENT_TIMESTAMP=$(date +%Y-%m-%d\_%H-%M-%S)
readonly SERVER_NAME=$(hostname -f)
readonly LOG_PATH="${PROGDIR}/output/log"
readonly LOG_FILE="${LOG_PATH}/${SERVER_NAME}_${CURRENT_TIMESTAMP}.txt"

#######################################
# Source code download
#######################################
function source_code_download {
  local source_path
  echo "==========================================================================="
  echo " Source code download Android"
  echo "==========================================================================="
  source_path="$TOPAZ_INPUT_DIR/android_source"
  rm -rf "$source_path"
  git clone -b "$GIT_ANDROID_BRANCH_NAME" "$GIT_ANDROID_REPOSITORY" "$source_path" > /dev/null

  echo "==========================================================================="
  echo " Source code download iOS"
  echo "==========================================================================="
  source_path="$TOPAZ_INPUT_DIR/ios_source"
  rm -rf "$source_path"
  git clone -b "$GIT_IOS_BRANCH_NAME" "$GIT_IOS_REPOSITORY" "$source_path" > /dev/null
}

#######################################
# Process every app
#######################################
function process_apps {
  # Nacteni konfiguracniho souboru, zpracovani vsech app pro vsechny podniky
  while IFS=";", read -r id iOsReleasedVersion androidReleasedVersion androidVersionCode brandName subdomain iOsBundleId androidPackageName; do
    if [ "$id" == "id" ] ; then
      continue
    fi
    echo "==========================================================================="
    echo " Processing  $id    $brandName    $subdomain"
    echo "   Android released version (name): $androidReleasedVersion"
    echo "                      version code: $androidVersionCode"
    echo "==========================================================================="
    bash "${PROGDIR}/topaz_build_android.sh" "$id" "$brandName" "$subdomain" "$androidVersionCode" "$androidReleasedVersion" "$androidPackageName"

 
  done < "$TOPAZ_CONFIG_FILE"
}

#######################################
# Vypise stav 
#######################################
function print_status() {
  clear
  local topaz_version_date
  topaz_version_date=$(stat -f "%Sm" -t "%Y-%m-%d %H:%M")
  echo "==========================================================================="
  echo "    _                        "
  echo "   | |                       "
  echo "   | |_ ___  _ __   __ _ ____"
  echo "   | __/ _ \| '_ \ / _\` |_ /"
  echo "   | || (_) | |_) | (_| |/ / "
  echo "   \__\___\/| .__\/\__,_/___|"
  echo "            | |              "
  echo "            |_|              "
  echo "==========================================================================="
  echo "  Topaz deploy (verze ${topaz_version_date})"
  echo "==========================================================================="   
}

#######################################
# Hlavni telo Topaz
#######################################
function main() {
  # Zmenime current working directory na cestu ke skriptu
  cd "$PROGDIR" || exit
  mkdir -p "${LOG_PATH}"

  # Beh vsech prikazu je logovan do Log souboru (a zaroven zobrazovan v terminale)
  print_status 2>&1 | tee -a "$LOG_FILE"
  # cmdline $ARGS 2>&1 | tee -a "$LOG_FILE"
  source_code_download | tee -a "$LOG_FILE"
  process_apps | tee -a "$LOG_FILE"
}
main
exit 0