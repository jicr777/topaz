#!/bin/bash
# strict mode
set -uo pipefail

if [ "$#" -eq  "0" ]; then
    echo "Debug build - testing branch"
    readonly BRANCH_ID="121"
    readonly BRANCH_NAME="Sport Cafe"
    readonly BRANCH_SUBDOMAIN="cz.mpizza.sportcafe"
    readonly BRANCH_VERSION_CODE="101"
    readonly BRANCH_VERSION_NAME="4.0"
    readonly BRANCH_ANDROID_PACKAGE_NAME="cz.mpizza.sportcafe"
elif [ "$#" -eq  "6" ]; then
    readonly BRANCH_ID="$1"
    readonly BRANCH_NAME="$2"
    readonly BRANCH_SUBDOMAIN="$3"
    readonly BRANCH_VERSION_CODE="$4"
    readonly BRANCH_VERSION_NAME="$5"
    readonly BRANCH_ANDROID_PACKAGE_NAME="$6"

else
    echo "Error 1: Not enough parameters."
    exit 1
fi

readonly DV_PACKAGE_NAME="${BRANCH_ANDROID_PACKAGE_NAME}"
readonly DV_APP_ID="${BRANCH_SUBDOMAIN}"
readonly DV_VERSION_CODE="${BRANCH_VERSION_CODE}"
readonly DV_VERSION_NAME="${BRANCH_VERSION_NAME}"
readonly DV_SUBDOMAIN="${BRANCH_SUBDOMAIN}"
readonly DV_APP_NAME="${BRANCH_NAME}" 
readonly PROGDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly BUILDPATH="${PROGDIR}/output/${BRANCH_ID}android"

#######################################
# Replace string in file (ver 1.3 2018 10 29)
# Arguments: 
#   find string
#   replace with string
#   file name
#######################################
function replace_in_file() {
  local find
  find=$(unregex "$1")
  local replace=$2 
  local file_name=$3
  if [ ! -f "$file_name" ]; then
    echo "CHYBA: Soubor pro nahrazeni nenalezen $file_name"
    return 1
  fi
  echo "Nahrazeni $find > $replace v $file_name"
  sed -i '' -e "s|$find|$replace|g" "$file_name"
}

#######################################
# Escape regex (ver 1.1 2018 10 29)
# Arguments: 
#   string to be escaped 
#######################################
function unregex {
   sed -e 's/[\/&]/\\&/g' <<< "$1"
}

# Copy source code for customization
rm -rf "$BUILDPATH"
mkdir -p "$BUILDPATH"
cp -r "${PROGDIR}/input/android_source/" "$BUILDPATH"

# Source Customization
replace_in_file "DV_APP_ID" "$DV_PACKAGE_NAME" "${BUILDPATH}/app/build.gradle"
replace_in_file "DV_VERSION_CODE" "$DV_VERSION_CODE" "${BUILDPATH}/app/build.gradle"
replace_in_file "DV_VERSION_NAME" "$DV_VERSION_NAME" "${BUILDPATH}/app/build.gradle"
replace_in_file "DV_SUBDOMAIN" "$DV_SUBDOMAIN" "${BUILDPATH}/app/src/main/java/com/deepvision/c2elauncher/MainActivity.java"
replace_in_file "DV_APP_NAME" "$DV_APP_NAME" "${BUILDPATH}/app/src/main/res/values/strings.xml"

# Icons
readonly INPUT_ICON_PATH="${PROGDIR}/input/icons/${BRANCH_ID}_icon_1024.png"
if [[ ! -f $INPUT_ICON_PATH ]]; then
	echo "ERROR: Could not find input icon file ${INPUT_ICON_PATH}"
else
  # Generate icons
  readonly MASTER_ICON_PATH="${BUILDPATH}/icons/icon_1024.png"
  mkdir -p "${BUILDPATH}/icons"
  cp -r "$INPUT_ICON_PATH" "$MASTER_ICON_PATH"
  bash "${PROGDIR}/topaz_generate_icons.sh" "$MASTER_ICON_PATH"
  # Copy icons to source
  cp -r "${BUILDPATH}/icons/Android/" "${BUILDPATH}/app/src/main/res/"
  cp -r "${BUILDPATH}/icons/Android/ic_launcher-web.png" "${BUILDPATH}/app/src/main/ic_launcher-web.png"
  
fi

# Fast lane config
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
cp -r "${PROGDIR}/input/android_fastline_config/" "$BUILDPATH"
replace_in_file "DV_PACKAGE_NAME" "${DV_PACKAGE_NAME}" "${BUILDPATH}/fastlane/Appfile"
    
# Fast lane execution
cd "$BUILDPATH" || exit
export ANDROID_HOME="${HOME}/Library/Android/sdk"
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
# fastlane supply init
bundle exec fastlane

