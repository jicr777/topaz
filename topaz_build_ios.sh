#!/bin/bash
# strict mode
set -uo pipefail
if [ "$#" -eq  "2" ]
then
    # readonly BRANCH_ID="${1}"
    # readonly BRANCH_NAME="${2}"
    
    readonly BRANCH_ID="16"
    readonly BRANCH_NAME="ureky"
    # The bundle identifier of app
    readonly APP_IDENTIFIER="ureky.app.id"
    # Apple ID email address
    readonly APPLE_ID="jirka.spacek@gmail.com"
    readonly PROGDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
else
    echo "Error 1: Not enough parameters."
    exit 1
fi

#######################################
# Replace string in file (ver 1.2)
# Arguments: 
#   find string
#   replace with string
#   file name
#######################################
function replace_in_file() {
  local find=$(unregex "$1")
  local replace=$2 
  local file_name=$3
  if [ ! -f "$file_name" ]; then
    echo "CHYBA: Soubor pro nahrazeni nenalezen $file_name"
    return 1
  fi
  echo "Nahrazeni $find > $replace v $file_name"
  sed -i '' -e "s|$find|$replace|g" "$file_name"
}

#######################################
# Escape regex
#######################################
function unregex {
   #sed -e 's/[]\/()$*.^|[]/\\&/g' <<< "$1"
   sed -e 's/[\/&]/\\&/g' <<< "$1"
}

# Copy source code for customization
readonly BUILDPATH="${PROGDIR}/output/${BRANCH_ID}ios"
rm -rf "$BUILDPATH"
mkdir -p "$BUILDPATH"
cp -r "${PROGDIR}/input/ios_source/" "$BUILDPATH"

# Icons
readonly INPUT_ICON_PATH="${PROGDIR}/input/icons/${BRANCH_ID}_icon_1024.png"
if [[ ! -f $INPUT_ICON_PATH ]]; then
	echo "ERROR: Could not find input icon file ${INPUT_ICON_PATH}"
else
  # Generate icons
  readonly MASTER_ICON_PATH="${BUILDPATH}/icons/icon_1024.png"
  mkdir -p "${BUILDPATH}/icons"
  cp -r "$INPUT_ICON_PATH" "$MASTER_ICON_PATH"
  bash "${PROGDIR}/topaz_generate_icons.sh" "$MASTER_ICON_PATH"
  # Copy icons to source
  cp -r "${BUILDPATH}/icons/iOS/" "${BUILDPATH}/cz.deepvision.c2e.test.a1/Assets.xcassets/AppIcon.appiconset"
fi

# Source Customization
# Info.plist Customization

# sed -i '' "s/DV_APP_ID/$BUNDLE/g" temp_"$ID"/mPizza_build/Config.xcconfig
# sed -i '' "s/DV_APP_NAME/$NAME_UTF/g" temp_"$ID"/mPizza_build/Config.xcconfig
# sed -i '' "s/DV_EXE_NAME/$NAME/g" temp_"$ID"/mPizza_build/Config.xcconfig
# sed -i '' "s/DV_WAC/$WAC/g" temp_"$ID"/mPizza_build/Config.xcconfig
# sed -i '' "s/DV_APP_VERSION/$VERSION/g" temp_"$ID"/mPizza_build/Config.xcconfig
# sed -i '' "s/DV_APP_BUILD/$BUILD/g" temp_"$ID"/mPizza_build/Config.xcconfig

# sed -i '' "s/DV_APP_ID/$BUNDLE/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig
# sed -i '' "s/DV_APP_NAME/$NAME_UTF/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig
# sed -i '' "s/DV_EXE_NAME/$NAME/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig
# sed -i '' "s/DV_WAC/$WAC/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig
# sed -i '' "s/DV_APP_VERSION/$VERSION/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig
# sed -i '' "s/DV_APP_BUILD/$BUILD/g" temp_"$ID"/mPizza_build/Config.debug.xcconfig


# Fast lane config
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
cp -r "${PROGDIR}/input/ios_fastline_config/" "$BUILDPATH"
replace_in_file "PLACEHOLDER_APP_IDENTIFIER" "${APP_IDENTIFIER}" "${BUILDPATH}/fastlane/Appfile"
replace_in_file "PLACEHOLDER_APPLE_ID" "${APPLE_ID}" "${BUILDPATH}/fastlane/Appfile"

# sed -i '' "s/DV_APP_ID/$BUNDLE/g" temp_"$ID"/fastlane/Appfile
# sed -i '' "s/DV_APP_ID/$BUNDLE/g" temp_"$ID"/fastlane/Deliverfile
# sed -i '' "s/DV_APP_VERSION/$VERSION/g" temp_"$ID"/fastlane/Fastfile
# sed -i '' "s/DV_APP_BUILD/$BUILD/g" temp_"$ID"/fastlane/Fastfile
# sed -i '' "s/DV_APP_LANG/$LANG/g" temp_"$ID"/fastlane/Snapfile

# Fast lane execution
cd "$BUILDPATH" || exit
# bundle exec fastlane

