#!/bin/bash

COMPRESS=0                # Compress if ImageOptim is intalled. 0 = disabled; 1 = enabled
iOSSubdir="/iOS"          # make this an empty string to save in same dir
androidSubdir="/Android"  # make this an empty string to save in same dir

help() {
	echo ""
	echo "Usage: ./$(basename "$0") <your_1024x1024.png>"
	echo "       --------------------------------------------------------------------------------------"
	echo "       If you don't pass a parameter, it will look for a iTunesArtwork@2x.png in the current"
	echo "       directory. The file needs to be 1024x1024 for the rest of the files to be resized"
	echo "       correctly."
	echo ""
	echo "       If you want to automatically compress the icons, install ImageOptim."
	echo "       More info here: http://imageoptim.com/"
	exit
}

FILEPATH=$1
if [[ ! -f $FILEPATH ]]; then
	FILEPATH="./iTunesArtwork@2x.png"
fi
if [[ ! -f $FILEPATH ]]; then
	echo "ERROR: Could not find icon file"
	help
fi
FILE=$(basename "$FILEPATH")

# the root directory is where the icon passed as a parameter lives
DIR=$(cd $(dirname "$FILEPATH"); pwd)

# make subdirs
if [[ $iOSSubdir != "" && ! -d ${DIR}/$iOSSubdir ]]; then mkdir ${DIR}/${iOSSubdir}; fi
if [[ $androidSubdir != "" && ! -d ${DIR}/$androidSubdir ]]; then mkdir ${DIR}/${androidSubdir}; fi

# iOS App Store Icons
readonly ICON_PREFIX="icon_holder"
if [[ $FILE != "./iTunesArtwork@2x.png" || $iOSSubdir != "" ]]; then sips --resampleWidth 1024 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/iTunesArtwork@2x.png" > /dev/null 2>&1; fi
sips --resampleWidth 1024 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}store_1024pt.png" > /dev/null 2>&1

# iOS App Icons
sips --resampleWidth 120 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneApp_60pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 180 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneApp_60pt@3x.png" > /dev/null 2>&1
sips --resampleWidth 76 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadApp_76pt.png" > /dev/null 2>&1
sips --resampleWidth 152 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadApp_76pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 167 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadProApp_83,5pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 20 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadNotifications_20pt.png" > /dev/null 2>&1
sips --resampleWidth 40 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadNotifications_20pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 20 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneNotification_20pt.png" > /dev/null 2>&1
sips --resampleWidth 40 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneNotification_20pt@2x.png" > /dev/null 2>&1

# iOS Search/Settings
sips --resampleWidth 29 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadSpootlight5_29pt.png" > /dev/null 2>&1
sips --resampleWidth 58 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadSpootlight5_29pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 40 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadSpootlight7_40pt.png" > /dev/null 2>&1
sips --resampleWidth 80 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPadSpootlight7_40pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 58 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneSpootlight5_29pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 87 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneSpootlight5_29pt@3x.png" > /dev/null 2>&1
sips --resampleWidth 80 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneSpootlight7_40pt@2x.png" > /dev/null 2>&1
sips --resampleWidth 120 "${DIR}/${FILE}" --out "${DIR}${iOSSubdir}/${ICON_PREFIX}iPhoneSpootlight7_40pt@3x.png" > /dev/null 2>&1


# Android icons
sips --resampleWidth 512 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/ic_launcher-web.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-xxxhdpi"
sips --resampleWidth 192 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxxhdpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 192 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxxhdpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 192 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxxhdpi/ic_launcher.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-xxhdpi"
sips --resampleWidth 144 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxhdpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 144 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxhdpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 144 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xxhdpi/ic_launcher.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-xhdpi"
sips --resampleWidth 96 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xhdpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 96 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xhdpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 96 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-xhdpi/ic_launcher.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-hdpi"
sips --resampleWidth 72 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-hdpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 72 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-hdpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 72 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-hdpi/ic_launcher.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-mdpi"
sips --resampleWidth 48 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-mdpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 48 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-mdpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 48 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-mdpi/ic_launcher.png" > /dev/null 2>&1

mkdir -p "${DIR}${androidSubdir}/mipmap-ldpi"
sips --resampleWidth 36 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-ldpi/ic_launcher_foreground.png" > /dev/null 2>&1
sips --resampleWidth 36 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-ldpi/ic_launcher_round.png" > /dev/null 2>&1
sips --resampleWidth 36 "${DIR}/${FILE}" --out "${DIR}${androidSubdir}/mipmap-ldpi/ic_launcher.png" > /dev/null 2>&1

echo "Your icons have been created!"

# optimize PNGs, if ImageOptim is installed
ImageOptim="/Applications/ImageOptim.app/Contents/MacOS/ImageOptim"
if [[ $COMPRESS -eq 1 ]]; then
	if [[ -f $ImageOptim ]]; then
		echo "Optimizing iOS icons... This is going to take a while..."
		$($ImageOptim ${DIR}${iOSSubdir}/*)
		echo "Optimizing Android icons... This is going to take a while..."
		$($ImageOptim ${DIR}${androidSubdir}/*)
		echo ""
		echo "Finished compressing the icon files!"
	else
		echo "Skipped compression. To compress the PNGs, install ImageOptim. More info here: http://imageoptim.com/"
	fi
fi
