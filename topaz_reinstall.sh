#!/bin/bash

# Git setup
# git config --global user.name "Jiri Spacek"
# git config --global user.email "jirka.spacek@gmail.com"
# git clone -b master https://bitbucket.org/deepvisioncz/doveze.cz-ios.git . 

echo "- Nainstalovat xcode z app store"

echo "- Nainstalovat Android studio"
echo "- Updatovat SDK v Android studiu (Welcome to Android studio > Configure > Check for updates)"

# Accept Android licences
cd ~/Library/Android/sdk/tools/bin || exit
./sdkmanager --licenses
# All licenses must be accepted (type y)

# Install the latest Xcode command line tools:
xcode-select --install

sudo gem install fastlane -NV

sudo gem install screengrab



# Homebrew + adb (Screenshoty)
#sudo ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#brew cask install android-platform-tools
#adb devices