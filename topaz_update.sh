#!/bin/bash
# strict mode
set -uo pipefail

echo "Updating Topaz from git"
readonly GIT_TOPAZ_REPOSITORY="git@bitbucket.org:jicr777/topaz.git"
readonly GIT_TOPAZ_BRANCH_NAME="master"
readonly PROGDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

readonly SELF_UPDATE_PATH="${PROGDIR}/output/topaz_self_update"
rm -rf "$SELF_UPDATE_PATH"
mkdir -p "$SELF_UPDATE_PATH"

git clone -b "$GIT_TOPAZ_BRANCH_NAME" "$GIT_TOPAZ_REPOSITORY" "$SELF_UPDATE_PATH" > /dev/null

cp "${SELF_UPDATE_PATH}/topaz.sh" "${PROGDIR}/topaz.sh"
cp "${SELF_UPDATE_PATH}/topaz_generate_icons.sh" "${PROGDIR}/topaz_generate_icons.sh"
cp "${SELF_UPDATE_PATH}/topaz_reinstall.sh" "${PROGDIR}/topaz_reinstall.sh"
cp "${SELF_UPDATE_PATH}/topaz_build_android.sh" "${PROGDIR}/topaz_build_android.sh"
cp "${SELF_UPDATE_PATH}/topaz_build_ios.sh" "${PROGDIR}/topaz_build_ios.sh"
cp "${SELF_UPDATE_PATH}/topaz_update.sh" "${PROGDIR}/topaz_update.sh"